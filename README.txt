This module allows a Drupal site administrator to encrypt all outgoing mail.

This module requires the GnuPG PHP extension from
pecl.php.net/package/gnupg Installation instructions that work for
CentOS can be found at
http://www.drupalfoo.com/drupal-commerce-gpg-php-gnupg-centos-cpanel-whm
For Debian, you can use:
http://blog.space2place.de/2013/12/debian-wheezy-php-und-gnupg/

There is currently no admin UI, you need to hardcode some values in
settings.php or a file included from there before you can install this
module.

Variables to use:

// gnupg_directory: a directory that will contain the keyring for the
// GnuPG extension. It needs to be readable and writeable by the
// apache user. If you use several webheads, the directory needs to be
// shared between them. The directory needs to be outside the webroot.
$conf['gnupg_directory'] = '/var/www/some_value';

// gnupg_enforce controlls wether the content of the mail will be sent
// unencrypted if there is no key found for a recipient.
// Valid values are TRUE and FALSE
$conf['gnupg_enforce'] = TRUE;

// gnupg_replace_title: if TRUE, replace the title of outgoing mails
// by "Update to post %nid"
// Valid values are TRUE and FALSE
$conf['gnupg_replace_title'] = TRUE;


After installation, you'll find that the module added a field to the
user account entity at /admin/config/people/accounts/fields
